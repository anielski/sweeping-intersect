#include "ViewModel/LineSegmentVM.h"

LineSegmentVM::LineSegmentVM(double x1, double y1, double x2, double y2)
    : pointA(PointVM(x1,y1)), pointB(PointVM(x2,y2)), lineGraphicItem(NULL), labelGraphicItem(NULL) {}

LineSegmentVM::LineSegmentVM(double x1vis, double y1vis, double x2vis, double y2vis, ScalingInfo *scalingInfo)
    : pointA(PointVM(x1vis,y1vis,scalingInfo)), pointB(PointVM(x2vis,y2vis,scalingInfo)), lineGraphicItem(NULL), labelGraphicItem(NULL) {}

void LineSegmentVM::drawShape(QGraphicsScene *scene, QPen &pen) {
    pointA.setColor(pen.color());
    pointB.setColor(pen.color());


    pen.setWidth(2);
    lineGraphicItem = scene->addLine(0,0,0,0, pen);
    lineGraphicItem->setPos(0,0);
    lineGraphicItem->setLine(pointA.getViewPosition().x(), pointA.getViewPosition().y(), pointB.getViewPosition().x(), pointB.getViewPosition().y());
    lineGraphicItem->setZValue(LINE_Z);
    pointA.draw(scene);
    pointB.draw(scene);
}

void LineSegmentVM::drawLabel(QGraphicsScene *scene) {
    QString label = getLabel();
    if(label != "") {
        labelGraphicItem = scene->addSimpleText(label);
        labelGraphicItem->setPos(computeLabelPos());
    }
}

void LineSegmentVM::eraseShape(QGraphicsScene *scene) {
    scene->removeItem(lineGraphicItem);
    pointA.erase(scene);
    pointB.erase(scene);

    delete lineGraphicItem;
    lineGraphicItem = NULL;
}

void LineSegmentVM::eraseLabel(QGraphicsScene *scene) {
    scene->removeItem(labelGraphicItem);
    delete labelGraphicItem;
    labelGraphicItem = NULL;
}

void LineSegmentVM::setSelected(bool selected) {
    AbstractGeoShapeViewModel::setSelected(selected);
    QPen pen = lineGraphicItem->pen();
    pen.setColor(selected ? QColor(SELECTION_COLOR) : getColor());
    lineGraphicItem->setPen(pen);
    pointA.setSelected(selected);
    pointB.setSelected(selected);
}

void LineSegmentVM::setLabelVisible(bool labelVisible) {
    AbstractGeoShapeViewModel::setLabelVisible(labelVisible);
    pointA.setLabelVisible(labelVisible);
    pointB.setLabelVisible(labelVisible);
}

bool LineSegmentVM::isObscuredBy(QGraphicsItem *item) {
    return lineGraphicItem->isObscuredBy(item)
            && pointA.isObscuredBy(item)
            && pointB.isObscuredBy(item);
}


QGraphicsItem *LineSegmentVM::geoShapeAt(QGraphicsScene *scene, QPointF &point) {
    if(lineGraphicItem) {

        QGraphicsItem * item = pointA.geoShapeAt(scene, point);
        if(!item) {
         item =  pointB.geoShapeAt(scene, point);
        }
        if(!item && scene->itemAt(point) == lineGraphicItem) {
         item = lineGraphicItem;
        }
        return item;

    }
    return NULL;
}

void LineSegmentVM::translate(QGraphicsItem* item, QPointF& diff, ScalingInfo *scalingInfo) {
    if(item == pointA.getPointGraphicItem()) {
        pointA.translate(item, diff, scalingInfo);
    } else if(item == pointB.getPointGraphicItem()) {
        pointB.translate(item, diff, scalingInfo);

    } else {
        pointA.translate(item, diff, scalingInfo);
        pointB.translate(item, diff, scalingInfo);
    }
    lineGraphicItem->setLine(pointA.getViewPosition().x(), pointA.getViewPosition().y(),
                             pointB.getViewPosition().x(), pointB.getViewPosition().y());
    if(labelGraphicItem) {
        labelGraphicItem->setPos(computeLabelPos());
    }
}

QPointF LineSegmentVM::computeLabelPos() {
    qreal textWidth = labelGraphicItem->boundingRect().width();
    qreal posX = pointA.getViewPosition().x() + (pointB.getViewPosition().x() - pointA.getViewPosition().x())/2.0 - textWidth/2.0;
    qreal posY = pointA.getViewPosition().y() + (pointB.getViewPosition().y() - pointA.getViewPosition().y())/2.0;


    return QPointF(posX, posY);
}

double LineSegmentVM::getTop() {
    if(pointA.getPosition().y() > pointB.getPosition().y()) return pointA.getPosition().y();
            else return pointB.getPosition().y();
}

double LineSegmentVM::getRight() {
    if(pointA.getPosition().x() > pointB.getPosition().x()) return pointA.getPosition().x();
            else return pointB.getPosition().x();
}

double LineSegmentVM::getBottom() {
    if(pointA.getPosition().y() < pointB.getPosition().y()) return pointA.getPosition().y();
            else return pointB.getPosition().y();
}

double LineSegmentVM::getLeft() {
    if(pointA.getPosition().x() < pointB.getPosition().x()) return pointA.getPosition().x();
            else return pointB.getPosition().x();
}

void LineSegmentVM::scaleViewPosition(double xMove, double yMove, double multiplier) {
    pointA.scaleViewPosition(xMove, yMove, multiplier);
    pointB.scaleViewPosition(xMove, yMove, multiplier);
}

QString LineSegmentVM::serialize() {
      QString serialized =  "SEGMENT\nCOLOR="
              + QString::number(this->getColor().red()) + " " + QString::number(this->getColor().green()) +
              " " + QString::number(this->getColor().blue()) + "\n" +
              "LABEL=" + this->getLabel() + "\n" +
              QString::number(pointA.getPosition().x()) + " " + QString::number(pointA.getPosition().y()) + "\n" +
              QString::number(pointB.getPosition().x()) + " " + QString::number(pointB.getPosition().y()) + "\n";
      return serialized;
}
