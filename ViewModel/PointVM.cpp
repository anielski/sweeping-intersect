#include "ViewModel/PointVM.h"
#include <iostream>

#define POINT_DIAMETER 5


PointVM::PointVM(double x, double y) {
    position = QPointF(x, y);
    viewPosition = QPointF(x, y);
    pointGraphicItem = NULL;
    labelGraphicItem = NULL;
}

PointVM::PointVM(double xVis, double yVis, ScalingInfo *scalingInfo) {
    viewPosition = QPointF(xVis, yVis);
    position = QPointF((xVis/scalingInfo->multiplier)-scalingInfo->xMove, (yVis*(-1)/scalingInfo->multiplier)-scalingInfo->yMove);
    pointGraphicItem = NULL;
    labelGraphicItem = NULL;
}

  void PointVM::drawShape(QGraphicsScene *scene, QPen& pen) {
      pointGraphicItem = scene->addEllipse(0,0,POINT_DIAMETER, POINT_DIAMETER, pen, QBrush(pen.color()));
      pointGraphicItem->setPos(viewPosition.x() - POINT_DIAMETER/2, viewPosition.y() - POINT_DIAMETER/2);
      pointGraphicItem->setZValue(POINT_Z);
 }

  void PointVM::drawLabel(QGraphicsScene *scene) {
      labelGraphicItem = scene->addSimpleText(createVisualLabel());
      qreal textWidth = labelGraphicItem->boundingRect().width();
      labelGraphicItem->setPos(viewPosition.x() + POINT_DIAMETER/2.0 - textWidth/2.0, viewPosition.y() + POINT_DIAMETER + LABEL_VMARGIN);
  }

  void PointVM::eraseShape(QGraphicsScene *scene) {
      scene->removeItem(pointGraphicItem);
      delete pointGraphicItem;
      pointGraphicItem = NULL;
  }

  void PointVM::eraseLabel(QGraphicsScene *scene) {
      scene->removeItem(labelGraphicItem);
      delete labelGraphicItem;
      labelGraphicItem = NULL;
  }

  bool PointVM::isObscuredBy(QGraphicsItem *item) {
      return pointGraphicItem->isObscuredBy(item);
  }

  QGraphicsItem* PointVM::geoShapeAt(QGraphicsScene *scene, QPointF &point) {
      if(pointGraphicItem && scene->itemAt(point) == pointGraphicItem) {
          return pointGraphicItem;
      }
      return NULL;
  }

  void PointVM::translate(QGraphicsItem *item, QPointF &diff, ScalingInfo *scalingInfo) {
      viewPosition.setX(viewPosition.x() + diff.x());
      viewPosition.setY(viewPosition.y() + diff.y());

      qreal newX = viewPosition.x();
      qreal newY = viewPosition.y();

      position.setX((newX/(scalingInfo->multiplier))-scalingInfo->xMove);
      position.setY((newY*(-1)/(scalingInfo->multiplier))-scalingInfo->yMove);

      setViewPosition(newX, newY);
      if(pointGraphicItem) {
        pointGraphicItem->moveBy(diff.x(), diff.y());
        if(isLabelVisible()) {
            labelGraphicItem->moveBy(diff.x(), diff.y());
            labelGraphicItem->setText(createVisualLabel());
        }
      }
  }

  QString PointVM::createVisualLabel() {
      QString posText = "(" + QString::number(position.x()) + ", " + QString::number(position.y()) + ")";

      if(getLabel() != "") {
          posText = getLabel() + "\n" + posText;
      }
      return posText;
  }

  double PointVM::getTop() {
      return position.y();
  }

  double PointVM::getRight() {
      return position.x();
  }

  double PointVM::getBottom() {
      return position.y();
  }

  double PointVM::getLeft() {
      return position.x();
  }

  void PointVM::setSelected(bool selected) {
      AbstractGeoShapeViewModel::setSelected(selected);
      QPen pen = pointGraphicItem->pen();
      QBrush brush = pointGraphicItem->brush();

      pen.setColor(selected ? QColor(SELECTION_COLOR) : getColor());
      brush.setColor(selected ? QColor(SELECTION_COLOR) : getColor());

      pointGraphicItem->setPen(pen);
      pointGraphicItem->setBrush(brush);
  }

  void PointVM::scaleViewPosition(double xMove, double yMove, double multiplier) {
      setViewPosition((position.x() + xMove)*multiplier, (position.y() + yMove)*multiplier*(-1));
  }

  QString PointVM::serialize() {
        QString serialized =  "POINT\nCOLOR="
                + QString::number(this->getColor().red()) + " " + QString::number(this->getColor().green()) +
                " " + QString::number(this->getColor().blue()) + "\n" +
                "LABEL=" + this->getLabel() + "\n" +
                QString::number(position.x()) + " " + QString::number(position.y()) + "\n";
        return serialized;
  }


