#ifndef STRAIGHTLINEVM_H
#define STRAIGHTLINEVM_H

#include <QtGui>
#include "ViewModel/LineSegmentVM.h"

class StraightLineVM : public LineSegmentVM
{
public:
    StraightLineVM(double A, double B, double C);
    StraightLineVM(double a, double b);
    StraightLineVM(double ax, double ay, double bx, double by);

    qreal visualValueAt(double x);
    qreal visualArgumentAt(double y);

    virtual bool isScalable() {
        return false;
    }

    void setA(double A) {
        this->A = A;
    }

    void setB(double B) {
        this->B = B;
    }

    void setC(double C) {
        this->C = C;
    }

    void moveVisual(QPointF &diff);
    void setPoints();

    virtual void translate(QGraphicsItem* item, QPointF& diff, ScalingInfo *scalingInfo);
    virtual void scaleViewPosition(double xMove, double yMove, double multiplier);

    virtual bool isObscuredBy(QGraphicsItem * item);
    virtual void setSelected(bool selected);

    virtual double getTop();
    virtual double getRight();
    virtual double getBottom();
    virtual double getLeft();

protected:
    virtual void drawShape(QGraphicsScene *scene, QPen &pen);

private:

    qreal minX, maxX, minY, maxY;
    double A, B, C;
    double xMove, yMove, multiplier;
};

#endif // STRAIGHTLINEVM_H
