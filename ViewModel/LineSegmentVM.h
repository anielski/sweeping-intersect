#ifndef LINESEGMENTVM_H
#define LINESEGMENTVM_H

#include "ViewModel/AbstractGeoShapeViewModel.h"
#include "ViewModel/PointVM.h"

class LineSegmentVM : public AbstractGeoShapeViewModel
{
public:
    LineSegmentVM(double x1, double y1, double x2, double y2);
    LineSegmentVM(double x1vis, double y1vis, double x2vis, double y2vis, ScalingInfo *scalingInfo);

    void setPointA(double x, double y) {
        pointA.setPosition(x, y);
    }

    void setPointA(PointVM& point) {
        pointA = point;
    }

     PointVM& getPointA() {
        return pointA;
    }

    void setPointB(PointVM& point) {
         pointB = point;
     }

    void setPointB(double x, double y) {
        pointB.setPosition(x,y);
    }

    PointVM& getPointB() {
        return pointB;
    }

    void setLineGraphicsItem(QGraphicsLineItem * item) {
        this->lineGraphicItem = item;
    }

    QGraphicsLineItem * getLineGraphicItem() {
        return lineGraphicItem;
    }

    QGraphicsSimpleTextItem * getLabelGraphicItem() {
        return labelGraphicItem;
    }

    virtual bool isObscuredBy(QGraphicsItem * item);
    virtual void setSelected(bool selected);
    virtual void setLabelVisible(bool labelVisible);

    virtual QGraphicsItem* geoShapeAt(QGraphicsScene * scene, QPointF &point);
    virtual void translate(QGraphicsItem* item, QPointF& diff, ScalingInfo *scalingInfo);

    virtual QString serialize();
    virtual double getTop();
    virtual double getRight();
    virtual double getBottom();
    virtual double getLeft();
    virtual void scaleViewPosition(double xMove, double yMove, double multiplier);

protected:

    QPointF computeLabelPos();

    virtual void drawShape(QGraphicsScene *scene, QPen &pen);
    virtual void drawLabel(QGraphicsScene *scene);
    virtual void eraseShape(QGraphicsScene *scene);
    virtual void eraseLabel(QGraphicsScene *scene);

private:

    QGraphicsLineItem * lineGraphicItem;
    QGraphicsSimpleTextItem * labelGraphicItem;
    PointVM pointA;
    PointVM pointB;
};

#endif // LINESEGMENTVM_H
