#ifndef POINTVIEWMODEL_H
#define POINTVIEWMODEL_H

#include "QtGui"
#include "ViewModel/AbstractGeoShapeViewModel.h"

class PointVM : public AbstractGeoShapeViewModel
{
public:
    PointVM(double x, double y);
    PointVM(double xVis, double yVis, ScalingInfo *scalingInfo);

    void setPosition(double x, double y) {
        this->position = QPointF(x, y);
    }

    QPointF& getPosition() {
        return this->position;
    }

    void setViewPosition(double x, double y) {
        this->viewPosition = QPointF(x, y);
    }

    QPointF& getViewPosition() {
        return this->viewPosition;
    }

    QGraphicsItem * getPointGraphicItem() {
        return pointGraphicItem;
    }

    virtual bool isObscuredBy(QGraphicsItem * item);
    virtual void setSelected(bool selected);

    virtual QGraphicsItem* geoShapeAt(QGraphicsScene * scene, QPointF &point);
    virtual void translate(QGraphicsItem* item, QPointF &diff, ScalingInfo *scalingInfo);

    virtual QString serialize();
    virtual double getTop();
    virtual double getRight();
    virtual double getBottom();
    virtual double getLeft();
    virtual void scaleViewPosition(double xMove, double yMove, double multiplier);

protected:
    virtual void drawShape(QGraphicsScene* scene, QPen& pen);
    virtual void drawLabel(QGraphicsScene *scene);
    virtual void eraseShape(QGraphicsScene *scene);
    virtual void eraseLabel(QGraphicsScene *scene);


private:
    QString createVisualLabel();
    QPointF position;
    QPointF viewPosition;
    QGraphicsEllipseItem * pointGraphicItem;
    QGraphicsSimpleTextItem * labelGraphicItem;
};

#endif // POINTVIEWMODEL_H
