#include "ViewModel/PolygonVM.h"

PolygonVM::PolygonVM() {}

PolygonVM::PolygonVM(vector<PointVM*> points)  {
    this->points = points;
    PolygonLineSegmentVM * prevLine;
    for(int i=0; i< points.size()-1; i++) {
        PolygonLineSegmentVM* newLine = new PolygonLineSegmentVM(points[i]->getViewPosition().x(), points[i]->getViewPosition().y(),
                                                   points[i+1]->getViewPosition().x(), points[i+1]->getViewPosition().y());
        lineSegs.push_back(newLine);
        if(prevLine) {
            prevLine->setNextSegment(newLine);
        }
        prevLine = newLine;
    }
    closePolygon();
}

void PolygonVM::addPoints(vector<QPointF> points) {

    if(lineSegs.size() > 0 || points.size() > 1) { //wielokat musi miec co najmniej 2 wierzcholki
    int k = 0;
    if(lineSegs.size() == 0) { //jesli dotychczas wielokat byl pusty
        QPointF pointA =  points[0];
        QPointF pointB = points[1];
         lineSegs.push_back(new PolygonLineSegmentVM(pointA.x(), pointA.y(), pointB.x(), pointB.y()));
         k+=2;
    } else if(lineSegs.size() > 1) { //jesli dotychczas wielokat skladal sie z wiecej 2 wierzcholkow - odmykamy ostatnia krawedz
        lineSegs.pop_back();
    }

        for(int i=k; i<points.size(); i++) {
            QPointF point = points[i];
            PolygonLineSegmentVM* prevSeg = lineSegs.back();
            QPointF prevPoint = prevSeg->getPointB().getPosition();
            PolygonLineSegmentVM* newLine = new PolygonLineSegmentVM(prevPoint.x(), prevPoint.y(), point.x(), point.y());
            lineSegs.push_back(newLine);
            prevSeg->setNextSegment(newLine);
        }

        closePolygon();

    }
}

bool PolygonVM::addPoint(QPointF point) { //ale musza byc co najmniej 2 wierzcholki juz !
    if(lineSegs.size() > 0) {
        if(lineSegs.size() > 1) {
          lineSegs.pop_back();
        }
        PolygonLineSegmentVM* prevSeg = lineSegs.back();
        QPointF prevPoint = prevSeg->getPointB().getPosition();
        PolygonLineSegmentVM* newLine = new PolygonLineSegmentVM(prevPoint.x(), prevPoint.y(), point.x(), point.y());
        lineSegs.push_back(newLine);
        prevSeg->setNextSegment(newLine);

        closePolygon();
        return true;
    }
    return false;
}

bool PolygonVM::removePoint(QPointF point) {
    if(lineSegs.size() > 1) {
        for(int i=0; i<lineSegs.size(); i++) {
            PolygonLineSegmentVM * current = lineSegs[i];
            QPointF p = current->getPointB().getPosition();
            if(p.x() == point.x() && p.y() == point.y()) {
                int indexNext = (i+1) % lineSegs.size();
                PolygonLineSegmentVM * next = lineSegs[indexNext];
                PolygonLineSegmentVM * toDelete;
                if(lineSegs.size() == 3) {
                    toDelete = lineSegs[i];
                    lineSegs.erase(lineSegs.begin() + i);
                    delete toDelete;

                    toDelete = lineSegs[i % lineSegs.size()];
                    lineSegs.erase(lineSegs.begin() + i % lineSegs.size());
                    delete toDelete;

                } else {
                    current->setPointB(next->getPointB().getPosition().x(), next->getPointB().getPosition().y());

                     toDelete = lineSegs[indexNext];
                     lineSegs.erase(lineSegs.begin() + indexNext);
                     delete toDelete;
                }

                return true;
            }
        }
    }
    return false;
}

void PolygonVM::closePolygon() {
    if(lineSegs.size() > 1) {
        PolygonLineSegmentVM* lastSeg = lineSegs.back();
        QPointF lastPoint = lastSeg->getPointB().getPosition();

        PolygonLineSegmentVM* firstSeg = lineSegs.front();
        QPointF firstPoint = firstSeg->getPointA().getPosition();

        PolygonLineSegmentVM * newSeg = new PolygonLineSegmentVM(lastPoint.x(), lastPoint.y(), firstPoint.x(), firstPoint.y());
        lineSegs.push_back(newSeg);
        lastSeg->setNextSegment(newSeg);
        newSeg->setNextSegment(firstSeg);
    }
}

void PolygonVM::drawShape(QGraphicsScene *scene, QPen &pen) {
    for(int i=0; i<lineSegs.size(); i++) {
        lineSegs[i]->setColor(pen.color());
        lineSegs[i]->draw(scene);
    }
}

void PolygonVM::drawLabel(QGraphicsScene *scene) {

}

void PolygonVM::eraseShape(QGraphicsScene *scene) {
    for(int i=0; i<lineSegs.size(); i++) {
        lineSegs[i]->erase(scene);
    }
}

void PolygonVM::eraseLabel(QGraphicsScene *scene) {

}

bool PolygonVM::isObscuredBy(QGraphicsItem *item) {

    for(int i=0; i< lineSegs.size(); i++) {
        if(!lineSegs[i]->isObscuredBy(item)) {
            return false;
        }
    }
    return true;
}


void PolygonVM::setSelected(bool selected) {
    AbstractGeoShapeViewModel::setSelected(selected);

    for(int i=0; i<lineSegs.size(); i++) {
        lineSegs[i]->setSelected(selected);
    }
}

void PolygonVM::setLabelVisible(bool labelVisible) {
    this->labelVisible = labelVisible;
    for(int i=0; i<lineSegs.size(); i++) {
        lineSegs[i]->setLabelVisible(labelVisible);
    }
}

QGraphicsItem * PolygonVM::geoShapeAt(QGraphicsScene *scene, QPointF &point) {
    for(int i=0; i<lineSegs.size(); i++) {
        QGraphicsItem * item = lineSegs[i]->geoShapeAt(scene, point);
        if(item) {
            return item;
        }
    }
    return NULL;
}

void PolygonVM::translate(QGraphicsItem *item, QPointF &diff, ScalingInfo *scalingInfo) {
     for(int i=0; i<lineSegs.size(); i++) {
         lineSegs[i]->translate(item,diff,scalingInfo);
     }
}

QString PolygonVM::serialize() {
      QString serialized =  "POLYGON\nCOLOR="
              + QString::number(this->getColor().red()) + " " + QString::number(this->getColor().green()) +
              " " + QString::number(this->getColor().blue()) + "\n" +
              "LABEL=" + this->getLabel() + "\n" + QString::number(lineSegs.size() + 1) + "\n";

      serialized += QString::number(lineSegs[0]->getPointA().getPosition().x()) + " "
                  + QString::number(lineSegs[0]->getPointA().getPosition().y()) + "\n";

      for(int i=0; i<lineSegs.size(); i++) {
            serialized += QString::number(lineSegs[i]->getPointB().getPosition().x()) + " "
                        + QString::number(lineSegs[i]->getPointB().getPosition().y()) + "\n";
      }
      return serialized;
}

PolygonVM::~PolygonVM() {
    for(int i=0; i<lineSegs.size(); i++) {
        delete lineSegs[i];
    }
}

double PolygonVM::getTop() {
    double top = numeric_limits<double>::min();
    for(int i=0; i<lineSegs.size(); i++) {
        double testTop = lineSegs[i]->getTop();
        if(testTop > top) top = testTop;
    }
    return top;
}

double PolygonVM::getRight() {
    double right = numeric_limits<double>::min();
    for(int i=0; i<lineSegs.size(); i++) {
        double testRight = lineSegs[i]->getRight();
        if(testRight > right) right = testRight;
    }
    return right;
}

double PolygonVM::getBottom() {
    double bottom = numeric_limits<double>::max();
    for(int i=0; i<lineSegs.size(); i++) {
        double testBottom = lineSegs[i]->getBottom();
        if(testBottom < bottom) bottom = testBottom;
    }
    return bottom;
}

double PolygonVM::getLeft() {
    double left = numeric_limits<double>::max();
    for(int i=0; i<lineSegs.size(); i++) {
        double testLeft = lineSegs[i]->getLeft();
        if(testLeft < left) left = testLeft;
    }
    return left;
}

void PolygonVM::scaleViewPosition(double xMove, double yMove, double multiplier) {
    for(int i=0; i<lineSegs.size(); i++) {
        lineSegs[i]->scaleViewPosition(xMove, yMove, multiplier);
    }
}
