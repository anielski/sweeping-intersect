#include "ViewModel/AbstractGeoShapeViewModel.h"

AbstractGeoShapeViewModel::AbstractGeoShapeViewModel() {
    color = QColor(0,0,0);
    label = "";
    id = globalId++;
    setLabelVisible(true);
    setSelected(false);
}

void AbstractGeoShapeViewModel::draw(QGraphicsScene * scene) {
    QPen pen = QPen(getColor());
    drawShape(scene, pen);
    if(isLabelVisible()) {
        drawLabel(scene);
    }
    setSelected(isSelected());
}

void AbstractGeoShapeViewModel::erase(QGraphicsScene *scene) {
    eraseShape(scene);
    if(isLabelVisible()) {
        eraseLabel(scene);
    }
}

int AbstractGeoShapeViewModel::globalId = 0;
