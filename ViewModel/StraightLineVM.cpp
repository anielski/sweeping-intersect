#include "ViewModel/StraightLineVM.h"
#include<iostream>
using namespace std;

StraightLineVM::StraightLineVM(double A, double B, double C)
    : A(A), B(B), C(C), LineSegmentVM(0,0,0,0)
{

}


StraightLineVM::StraightLineVM(double a, double b)
    : A(a), B(-1), C(b), LineSegmentVM(0,0,0,0)
{

}

StraightLineVM::StraightLineVM(double ax, double ay, double bx, double by)
    : B(-1), LineSegmentVM(0,0,0,0)
{
    A = (ay - by) / (ax - bx);
    C = ay - A * ax;
}

void StraightLineVM::drawShape(QGraphicsScene *scene, QPen &pen) {
   QRectF bounds = scene->sceneRect();

   minX = bounds.left();
   maxX = bounds.right();
   minY = bounds.top();
   maxY = bounds.bottom();

   setPoints();

   getPointA().setLabelVisible(false);
   getPointB().setLabelVisible(false);

   LineSegmentVM::drawShape(scene, pen);
   scene->removeItem(getPointA().getPointGraphicItem());
   scene->removeItem(getPointB().getPointGraphicItem());
}

void StraightLineVM::setPoints() {
    qreal leftX, leftY, rightX, rightY;

    if (B == 0) {
        leftX = (-C/A + xMove) * multiplier;
        leftY = minY;
        rightX = leftX;
        rightY = maxY;
    } else if (A == 0) {
        leftX = minX;
        leftY = (C/B - yMove) * multiplier;
        rightX = maxX;
        rightY = leftY;
    } else if(-A/B < 0) {
        leftX = qMax(visualArgumentAt(maxY), minX);
        leftY = visualValueAt(leftX);

        rightX = qMin(visualArgumentAt(minY), maxX);
        rightY = visualValueAt(rightX);
    } else {
        leftX = qMax(visualArgumentAt(minY), minX);
        leftY = visualValueAt(leftX);

        rightX = qMin(visualArgumentAt(maxY), maxX);
        rightY = visualValueAt(rightX);
    }

    getPointA().setViewPosition(leftX, leftY);
    getPointB().setViewPosition(rightX, rightY);

    setPointA(leftX/multiplier - xMove, leftY/multiplier - yMove);
    setPointB(rightX/multiplier - xMove, rightY/multiplier - yMove);
}

qreal StraightLineVM::visualValueAt(double x) {
    return A/B * x - (A/B * xMove - C/B  + yMove) * multiplier;
}

qreal StraightLineVM::visualArgumentAt(double y) {
    return B/A * y + (B/A * yMove - C/A  + xMove) * multiplier;
}

void StraightLineVM::moveVisual(QPointF &diff) {
    this->C -= A * diff.x()/multiplier - B * diff.y()/multiplier;
}

bool StraightLineVM::isObscuredBy(QGraphicsItem *item) {
    return false;
}

void StraightLineVM::translate(QGraphicsItem *item, QPointF &diff, ScalingInfo *scalingInfo) {
    if(item == getLineGraphicItem()) {
        moveVisual(diff);

        setPoints();

        getLineGraphicItem()->setLine(getPointA().getViewPosition().x(), getPointA().getViewPosition().y(),
                                 getPointB().getViewPosition().x(), getPointB().getViewPosition().y());
        if(getLabelGraphicItem()) {
            getLabelGraphicItem()->setPos(computeLabelPos());
        }
    }
}

void StraightLineVM::scaleViewPosition(double xMove, double yMove, double multiplier) {
    this->xMove = xMove;
    this->yMove = yMove;
    this->multiplier = multiplier;
}

void StraightLineVM::setSelected(bool selected) {
   LineSegmentVM::setSelected(selected);
}

double StraightLineVM::getTop() {
    return LineSegmentVM::getTop();
}

double StraightLineVM::getRight() {
    return LineSegmentVM::getRight();
}

double StraightLineVM::getBottom() {
    return LineSegmentVM::getBottom();
}

double StraightLineVM::getLeft() {
    return LineSegmentVM::getLeft();
}
