#ifndef POLYGONVM_H
#define POLYGONVM_H

#include <vector>
#include <limits>
#include "ViewModel/AbstractGeoShapeViewModel.h"
#include "ViewModel/PointVM.h"
#include "ViewModel/PolygonLineSegmentVM.h"

using namespace std;

class PolygonVM : public AbstractGeoShapeViewModel
{
public:
    PolygonVM();
    PolygonVM(vector<PointVM*> points);

    void addPoints(vector<QPointF> points);
    bool addPoint(QPointF point);
    bool removePoint(QPointF point);

    virtual bool isObscuredBy(QGraphicsItem * item);
    virtual void setSelected(bool selected);
    virtual void setLabelVisible(bool labelVisible);
    virtual QGraphicsItem* geoShapeAt(QGraphicsScene * scene, QPointF &point);
    virtual void translate(QGraphicsItem* item, QPointF &diff, ScalingInfo *scalingInfo);

    virtual QString serialize();
    virtual double getTop();
    virtual double getRight();
    virtual double getBottom();
    virtual double getLeft();
    virtual void scaleViewPosition(double xMove, double yMove, double multiplier);

    virtual ~PolygonVM();

protected:
    virtual void drawShape(QGraphicsScene* scene, QPen& pen);
    virtual void drawLabel(QGraphicsScene *scene);
    virtual void eraseShape(QGraphicsScene *scene);
    virtual void eraseLabel(QGraphicsScene *scene);


private:

    void closePolygon();

    vector<PointVM*> points;
    vector<PolygonLineSegmentVM*> lineSegs;
};

#endif // POLYGONVM_H
