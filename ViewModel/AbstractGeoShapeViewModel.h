#ifndef ABSTRACTGEOSHAPEVIEWMODEL_H
#define ABSTRACTGEOSHAPEVIEWMODEL_H

#include <QtGui>
#include <View/ScalingInfo.h>

#define POINT_Z 1.0
#define LINE_Z 0.8
#define SELECTION_RECT_Z 2.0

#define LABEL_VMARGIN 2
#define SELECTION_COLOR Qt::magenta

class GeometryModel;

class AbstractGeoShapeViewModel
{
public:
    AbstractGeoShapeViewModel();

    void setColor(QColor color) {
        this->color = color;
    }

    QColor getColor() {
        return this->color;
    }

    void setLabel(QString label) {
        this->label = label;
    }

    QString getLabel() {
        return this->label;
    }

    void virtual setLabelVisible(bool labelVisible) {
        this->labelVisible = labelVisible;
    }

    bool isLabelVisible() {
        return labelVisible;
    }

    int getId() {
        return id;
    }

    virtual void setSelected(bool selected) {
        this->selected = selected;
    }

    bool isSelected() {
        return this->selected;
    }

    virtual bool isScalable() {
        return true;
    }


    void draw(QGraphicsScene* scene);
    void erase(QGraphicsScene* scene);

    virtual bool isObscuredBy(QGraphicsItem * item) = 0;
    virtual QGraphicsItem* geoShapeAt(QGraphicsScene * scene, QPointF& point) = 0;
    virtual void translate(QGraphicsItem* item, QPointF& diff, ScalingInfo *scalingInfo) = 0;
    virtual QString serialize() = 0;

    virtual double getTop() = 0;
    virtual double getRight() = 0;
    virtual double getBottom() = 0;
    virtual double getLeft() = 0;
    virtual void scaleViewPosition(double xMove, double yMove, double multiplier) = 0;

protected:
    virtual void drawShape(QGraphicsScene*  scene, QPen& pen) = 0;
    virtual void drawLabel(QGraphicsScene*  scene) = 0;
    virtual void eraseShape(QGraphicsScene*  scene) = 0;
    virtual void eraseLabel(QGraphicsScene*  scene) = 0;

     bool labelVisible;

private:
    bool selected;
    static int globalId;
    int id;
    QColor color;
    QString label;


};

#endif // ABSTRACTGEOSHAPEVIEWMODEL_H
