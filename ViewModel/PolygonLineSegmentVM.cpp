#include "PolygonLineSegmentVM.h"


void PolygonLineSegmentVM::drawShape(QGraphicsScene *scene, QPen &pen) {
    LineSegmentVM::drawShape(scene, pen);
    scene->removeItem(getPointA().getPointGraphicItem());
}

void PolygonLineSegmentVM::eraseShape(QGraphicsScene *scene) {
    scene->removeItem(getLineGraphicItem());
    getPointB().erase(scene);

    delete getLineGraphicItem();
    setLineGraphicsItem(NULL);
}

void PolygonLineSegmentVM::setSelected(bool selected) {
    AbstractGeoShapeViewModel::setSelected(selected);
    QPen pen = getLineGraphicItem()->pen();
    pen.setColor(selected ? QColor(SELECTION_COLOR) : getColor());
    getLineGraphicItem()->setPen(pen);
    getPointB().setSelected(selected);
}

bool PolygonLineSegmentVM::isObscuredBy(QGraphicsItem *item) {
    return getLineGraphicItem()->isObscuredBy(item) && getPointB().isObscuredBy(item);
}

void PolygonLineSegmentVM::translate(QGraphicsItem *item, QPointF &diff, ScalingInfo *scalingInfo) {
    PointVM& pointA = getPointA();
    PointVM& pointB = getPointB();

    if(item == pointB.getPointGraphicItem()) {
        pointB.translate(item, diff, scalingInfo);
        if(nextSegment) {
            nextSegment->getPointA().translate(item, diff, scalingInfo);
        }

    } else if(dynamic_cast<QGraphicsLineItem*>(item)) {
        pointA.translate(item, diff, scalingInfo);
        pointB.translate(item, diff, scalingInfo);
    }
    getLineGraphicItem()->setLine(pointA.getViewPosition().x(), pointA.getViewPosition().y(),
                                  pointB.getViewPosition().x(), pointB.getViewPosition().y());
    if(getLabelGraphicItem()) {
        getLabelGraphicItem()->setPos(computeLabelPos());
    }
}
