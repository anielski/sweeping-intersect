#ifndef POLYGONLINESEGMENT_H
#define POLYGONLINESEGMENT_H

#include "ViewModel/LineSegmentVM.h"

class PolygonLineSegmentVM : public LineSegmentVM
{
public:
    PolygonLineSegmentVM(double x1, double y1, double x2, double y2) : LineSegmentVM(x1,y1,x2,y2) {}


    void setNextSegment( PolygonLineSegmentVM * nextSegment) {
        this->nextSegment = nextSegment;
    }

    PolygonLineSegmentVM * getNextSegment() {
        return this->nextSegment;
    }

    virtual bool isObscuredBy(QGraphicsItem * item);
    virtual void setSelected(bool selected);
    virtual void translate(QGraphicsItem *item, QPointF &diff, ScalingInfo * scalingInfo);

protected:
    virtual void drawShape(QGraphicsScene *scene, QPen &pen);
    virtual void eraseShape(QGraphicsScene *scene);

private:
        PolygonLineSegmentVM * nextSegment;
};

#endif // POLYGONLINESEGMENT_H
