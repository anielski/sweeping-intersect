#ifndef FILEPARSER_H
#define FILEPARSER_H

#include<iostream>
#include "ViewModel/PointVM.h"
#include "ViewModel/PolygonVM.h"
#include "ViewModel/LineSegmentVM.h"

using namespace std;

class FileParser
{
public:
    FileParser(QFile &file);
    PointVM * parsePoint();
    LineSegmentVM * parseSegment();
    PolygonVM * parsePolygon();

    QColor parseColor();
    QString parseLabel();
    QStringList parseCords();

    ~FileParser() {}

private:
    QFile * file;
};

#endif // FILEPARSER_H
