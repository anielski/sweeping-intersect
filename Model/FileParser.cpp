#include "FileParser.h"


FileParser::FileParser(QFile &file) {
    this->file = &file;
}

PointVM * FileParser::parsePoint() {
    QColor color = this->parseColor();
    QString label = this->parseLabel();
    QStringList cords = this->parseCords();

    PointVM *point = new PointVM(cords.at(0).toDouble(), cords.at(1).toDouble());

    point->setColor(color);
    point->setLabel(label);

    return point;
}

LineSegmentVM * FileParser::parseSegment() {
    QColor color = this->parseColor();
    QString label = this->parseLabel();
    QStringList cordsStart = this->parseCords();
    QStringList cordsEnd = this->parseCords();

    LineSegmentVM * segment = new LineSegmentVM(cordsStart.at(0).toDouble(), cordsStart.at(1).toDouble(), cordsEnd.at(0).toDouble(), cordsEnd.at(1).toDouble());

    segment->setColor(color);
    segment->setLabel(label);

    return segment;
}

PolygonVM * FileParser::parsePolygon() {
    QColor color = this->parseColor();
    QString label = this->parseLabel();

    QString line = file->readLine();
    int size = line.toInt();

    QStringList cords;
    vector<QPointF> vect;

    for (int i = 0; i < size; i++) {
        cords = this->parseCords();
        QPointF point(cords.at(0).toDouble(), cords.at(1).toDouble());
        vect.push_back(point);
    }

    PolygonVM * polygon = new PolygonVM();
    polygon->addPoints(vect);
    polygon->setColor(color);
    polygon->setLabel(label);


    return polygon;
}

QStringList FileParser::parseCords() {
    QString cords = file->readLine();

    QStringList cordsArray = cords.split(' ');

    return cordsArray;
}

QColor FileParser::parseColor() {
    QString colorLine = file->readLine();

    QStringList colorArray = colorLine.split('=');

    QStringList color = colorArray.at(1).split(' ');
    QColor pointColor(color.at(0).toInt(), color.at(1).toInt(), color.at(2).toInt());

    return pointColor;
}

QString FileParser::parseLabel() {
    QString LabelLine = file->readLine();

    QString label = LabelLine.split('=').at(1);

    return label;
}
