#include "Generator.h"

Generator::Generator()
{
}

vector<PointVM*> * Generator::generateInterval(int howMany, double left, double right, double bottom, double up) {
    srand(time(NULL));

    vector<PointVM *> * points = new vector<PointVM *>();

    for(int i=0; i<howMany; i++) {
       double x = (rand()/(double)(RAND_MAX + 1)) * (right-left) +left;
       double y = (rand()/(double)(RAND_MAX + 1)) * (up-bottom) +bottom;
       PointVM * p = new PointVM(x,y);
       points->push_back(p);
    }

    return points;
}

vector<PointVM *> * Generator::generateOnCircuit(int howMany, double ax, double ay, double bx, double by, double cx, double cy, double dx, double dy) {
    srand(time(NULL));

    double place, x, y;
    int c;

    vector<PointVM *> * points = new vector<PointVM *>();

    for (int i = 0; i < howMany; i++) {
        c = rand();
        place = rand()/(double)(RAND_MAX);

        if (c % 4 == 0) {
            x =  place * (bx - ax) + ax;
            y =  place * (by - ay) + ay;
        } else if (c % 4 == 1) {
            x =  place * (cx - bx) + bx;
            y =  place * (cy - by) + by;
        } else if (c % 4 == 2) {
            x =  place * (dx - cx) + cx;
            y =  place * (dy - cy) + cy;
        } else if (c % 4 == 3) {
            x =  place * (ax - dx) + dx;
            y =  place * (ay - dy) + dy;
        }

        points->push_back(new PointVM(x,y));
    }
    return points;
}

vector<PointVM *> * Generator::generateOnCircle(int howMany, double centerX, double centerY, double radius) {
    srand(time(NULL));

    vector<PointVM *> * points = new vector<PointVM *>();
    double fi, x, y;

    for(int i = 0; i < howMany; i++) {
        fi = (rand()/(double)(RAND_MAX)) * 2 * M_PI;
        x = radius * sin(fi) + centerX;
        y = radius * cos(fi) + centerY;

        points->push_back(new PointVM(x, y));
    }
    return points;
}


vector<PointVM *> * Generator::generateOnLine(int howMany, double minCord, double maxCord, double ax, double ay, double bx, double by){
    srand(time(NULL));

    double a = (by - ay) / (bx - ax);
    double b = by - a * bx;

    vector<PointVM *> * points = new vector<PointVM *>();

    for(int i = 0; i < howMany; i++) {
        double x = (rand()/(double)(RAND_MAX)) * (maxCord - minCord) + minCord;
        double y = a * x + b;

        points->push_back(new PointVM(x,y));
    }

    return points;
}

vector<PointVM *> * Generator::generateOnSquareAndAxes(int howManyOnAxes, int howManyOnDiagonal, double ax, double ay, double bx, double by, double cx, double cy, double dx, double dy) {
    srand(time(NULL));

    vector<PointVM *> * points = new vector<PointVM *>();

    points->push_back(new PointVM(ax, ay));
    points->push_back(new PointVM(bx, by));
    points->push_back(new PointVM(cx, cy));
    points->push_back(new PointVM(dx, dy));

    double place, x, y;

    for (int i = 0; i < howManyOnAxes; i++) {
        place = rand()/(double)(RAND_MAX);
        x = place * (bx - ax) + ax;

        place = rand()/(double)(RAND_MAX);
        y = place * (dy - ay) + ay;

        points->push_back(new PointVM(x, 0));
        points->push_back(new PointVM(0, y));
    }

    for (int i = 0; i < howManyOnDiagonal; i++) {
        place = rand()/(double)(RAND_MAX);
        x =  place * (cx - ax) + ax;
        y =  place * (cy - ay) + ay;

        points->push_back(new PointVM(x, y));

        place = rand()/(double)(RAND_MAX);
        x =  place * (dx - bx) + bx;
        y =  place * (dy - by) + by;

        points->push_back(new PointVM(x, y));
    }

    return points;
}
