#ifndef GEOMETRYMODEL_H
#define GEOMETRYMODEL_H

#include <vector>
#include <map>
#include <iterator>

#include "View/GeometryView.h"
#include "ViewModel/AbstractGeoShapeViewModel.h"
#include "ViewModel/PointVM.h"

#include <iostream>

using namespace std;

class GeometryView;

class GeometryModel : public QObject
{
    Q_OBJECT

public:
    GeometryModel();
    ~GeometryModel();

    void addGeoShape(AbstractGeoShapeViewModel* geoShape);
    void addPoints(vector<PointVM *> * points);
    bool removeGeoShape(AbstractGeoShapeViewModel* geoShape);
    vector<AbstractGeoShapeViewModel*> getGeoShapes();
    void clearModel();
    void clearMemory();

    void registerGeometryListener(GeometryView *listener);

    void performViewUpdate();

private:
    vector<AbstractGeoShapeViewModel*> geoShapes;
    map<AbstractGeoShapeViewModel*,int> shapeIndexes;

signals:
    void fireGeometryChange();
};

#endif // GEOMETRYMODEL_H
