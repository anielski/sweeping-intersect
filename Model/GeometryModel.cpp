#include "Model/GeometryModel.h"

GeometryModel::GeometryModel()
{
}

GeometryModel::~GeometryModel()
{
    clearMemory();
}

vector<AbstractGeoShapeViewModel *> GeometryModel::getGeoShapes() {
    return geoShapes;
}



void GeometryModel::clearModel() {
    geoShapes.clear();
}

void GeometryModel::clearMemory() {
    while(!geoShapes.empty()) {
        delete geoShapes.back();
        geoShapes.pop_back();
    }
}

void GeometryModel::addGeoShape(AbstractGeoShapeViewModel *geoShape) {
    geoShapes.push_back(geoShape);
    shapeIndexes[geoShape] = geoShapes.size() -1;
}

bool GeometryModel::removeGeoShape(AbstractGeoShapeViewModel *geoShape) {
    if(shapeIndexes.count(geoShape)) {
        geoShapes.erase(geoShapes.begin() + shapeIndexes[geoShape]);
        for(int i = shapeIndexes[geoShape]; i< geoShapes.size(); i++) {
            shapeIndexes[geoShapes[i]] -= 1;

        }
        shapeIndexes.erase(geoShape);
        return true;
    }
    return false;
}


void GeometryModel::registerGeometryListener(GeometryView *listener) {
    QObject::connect(this, SIGNAL(fireGeometryChange()), listener, SLOT(geometryChanged()));
}

void GeometryModel::performViewUpdate() {
    emit fireGeometryChange();
}

void GeometryModel::addPoints(vector<PointVM *> * points) {
    for (int i=0; i < points->size(); i++) {
        GeometryModel::addGeoShape((*points)[i]);
    }
}
