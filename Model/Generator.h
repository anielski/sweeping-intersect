#ifndef GENERATOR_H
#define GENERATOR_H
#include <cmath>
#define _USE_MATH_DEFINES
#include <vector>
#include <iostream>
#include <ctime>

#include "ViewModel/PointVM.h"

using namespace std;

class Generator
{
public:
    Generator();
    static vector<PointVM *> * generateInterval(int howMany, double left, double right, double bottom, double up);
    static vector<PointVM *> * generateOnCircuit(int howMany, double ax, double ay, double bx, double by,
                                              double cx, double cy, double dx, double dy);
    static vector<PointVM *> * generateOnCircle(int howMany, double centerX, double centerY, double radius);
    static vector<PointVM *> * generateOnLine(int howMany, double minCord, double maxCord,
                                              double ax, double ay, double bx, double by);
    /**
      Radosnie zakladamy, ze dwa boki kwadratu zawsze sa na osiach
      **/
    static vector<PointVM *> * generateOnSquareAndAxes(int howManyOnAxes, int howManyOnDiagonal, double ax, double ay, double bx, double by,
                                                      double cx, double cy, double dx, double dy);
};

#endif // GENERATOR_H
