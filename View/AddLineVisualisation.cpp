#include "AddLineVisualisation.h"

AddLineVisualisation::AddLineVisualisation(GeometryModel *model, QGraphicsScene *scene, bool areLabelsShown, ScalingInfo *scalingInfo)
    : model(model), scene(scene), areLabelsShown(areLabelsShown), scalingInfo(scalingInfo) {
    currentInitPoint = NULL;
    currentLineSeg = NULL;
}

void AddLineVisualisation::initNewLine(QPointF initPoint) {
    currentInitPoint = new PointVM(initPoint.x(), initPoint.y(), scalingInfo);
    currentInitPoint->setLabelVisible(areLabelsShown);
    model->addGeoShape(currentInitPoint);
    currentInitPoint->draw(scene);
}

void AddLineVisualisation::drag(QPointF point) {
    if(currentInitPoint->getPointGraphicItem()) {
        currentInitPoint->erase(scene);
        QPointF initPos = currentInitPoint->getViewPosition();
        currentLineSeg = new LineSegmentVM(initPos.x(), initPos.y(), point.x(), point.y(), scalingInfo);
        currentLineSeg->setLabelVisible(areLabelsShown);

        model->removeGeoShape(currentInitPoint);
        model->addGeoShape(currentLineSeg);

        currentLineSeg->draw(scene);

    } else {
        PointVM& pointB = currentLineSeg->getPointB();
        QPointF diff(point.x() - pointB.getViewPosition().x(), point.y() - pointB.getViewPosition().y());
        currentLineSeg->translate(currentLineSeg->getPointB().getPointGraphicItem(),diff,scalingInfo);
    }
}

AddLineVisualisation::~AddLineVisualisation() {
    if(currentLineSeg) {
        delete currentInitPoint;
    }
}
