#include<iostream>
#include <QtGui>

#include "View/MainWindow.h"
#include "ui_mainwindow.h"
#include "ViewModel/PointVM.h"
#include "Model/FileParser.h"
#include "View/SaveDialog.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)  {
     setupUi(this);
     generatorDialog = new GeneratorDialog(this);
}

void MainWindow::setGeometryModel(GeometryModel* model)  {
    geometryModel = model;
    geometryGraphicsView->setGeometryModel(model);
    model->registerGeometryListener(geometryGraphicsView);

   createActions();

    setCurrentFile("");
}

MainWindow::~MainWindow()
{
    //delete ui;
    delete generatorDialog;
}

void MainWindow::open()
 {
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty()) {
        loadFile(fileName);
    }
 }

 bool MainWindow::save()
 {
     if (curFile.isEmpty()) {
         return saveAs();
     } else {
         return saveFile(curFile);
     }
 }

 bool MainWindow::saveAs()
{
     QString fileName = QFileDialog::getSaveFileName(this);
     if (fileName.isEmpty())
         return false;

     return saveFile(fileName);
}

 void MainWindow::loadFile(const QString &fileName)
  {
      QFile file(fileName);
      if (!file.open(QFile::ReadOnly | QFile::Text)) {
          QMessageBox::warning(this, tr("Application"),
                               tr("Cannot read file %1:\n%2.")
                               .arg(fileName)
                               .arg(file.errorString()));
          return;
      }

      FileParser * parser = new FileParser(file);

      GeometryModel * model = geometryGraphicsView->getGeometryModel();
      model->clearModel();

      while(!file.atEnd()){
          QString line = file.readLine();

          if (line.contains("POINT")) {
                PointVM * point = parser->parsePoint();
                model->addGeoShape(point);
          } else if (line.contains("SEGMENT")) {
                LineSegmentVM * segment = parser->parseSegment();
                model->addGeoShape(segment);
          } else if (line.contains("POLYGON")) {
                PolygonVM * poly = parser->parsePolygon();
                model->addGeoShape(poly);
          }
      }

      delete parser;
      file.close();

      model->performViewUpdate();
      setCurrentFile(fileName);
  }

 bool MainWindow::saveFile(const QString &fileName)
  {
      QFile file(fileName);
      if (!file.open(QFile::WriteOnly | QFile::Text)) {
          QMessageBox::warning(this, tr("Application"),
                               tr("Cannot write file %1:\n%2.")
                               .arg(fileName)
                               .arg(file.errorString()));
          return false;
      }

      QTextStream out(&file);
      file.flush();

      vector<AbstractGeoShapeViewModel*> shapes = geometryGraphicsView->getGeometryModel()->getGeoShapes();
      for (int i = 0; i < shapes.size(); i++) {
            out << shapes[i]->serialize();
      }

      file.close();

      setCurrentFile(fileName);
      return true;
  }

 void MainWindow::setCurrentFile(const QString &fileName)
  {
      curFile = fileName;
      setWindowModified(false);

      QString shownName = curFile;
      if (curFile.isEmpty())
          shownName = "untitled.txt";
      setWindowFilePath(shownName);
  }

 void MainWindow::showGeneratedShapes() {
     vector<AbstractGeoShapeViewModel*> geoShapes = generatorDialog->getResults();
     for(int i=0; i< geoShapes.size(); i++) {
         geometryModel->addGeoShape(geoShapes[i]);
     }

     geometryModel->performViewUpdate();
 }

 void MainWindow::createActions()
 {
     actOpen->setShortcuts(QKeySequence::Open);
     actOpen->setStatusTip(tr("Open an existing file"));
     connect(actOpen, SIGNAL(triggered()), this, SLOT(open()));


     actSave->setShortcuts(QKeySequence::Save);
     actSave->setStatusTip(tr("Save the document to disk"));
     connect(actSave, SIGNAL(triggered()), this, SLOT(save()));

     actSave_As->setShortcuts(QKeySequence::SaveAs);
     actSave_As->setStatusTip(tr("Save the document under a new name"));
     connect(actSave_As, SIGNAL(triggered()), this, SLOT(saveAs()));

     connect(showLabelsCheckBox,SIGNAL(toggled(bool)),geometryGraphicsView,SLOT(showLabels(bool)));

     deleteSelectedShapesButton->setShortcut(QKeySequence::Delete);
     deleteSelectedShapesButton->adjustSize();
     connect(deleteSelectedShapesButton, SIGNAL(clicked()), geometryGraphicsView, SLOT(deleteSelectedShapes()));
     connect(geometryGraphicsView, SIGNAL(enableDeletingShapes(bool)), deleteSelectedShapesButton, SLOT(setEnabled(bool)));
     connect(genShapesButton, SIGNAL(clicked()), generatorDialog, SLOT(open()));
    genShapesButton->adjustSize();
     connect(generatorDialog, SIGNAL(shapesGenerated()), this, SLOT(showGeneratedShapes()));
 }


 void MainWindow::on_actionSave_as_image_triggered()
 {
     SaveDialog fileDialog(this);
     QStringList nameFilters;
     nameFilters<<"PNG (*.png)"
               <<"PDF (*.pdf)"
              <<"JPG (*.jpg)"
             <<"BMP (*.bmp)";
     fileDialog.setNameFilters(nameFilters);
     if(fileDialog.exec()) {
         QString filename = fileDialog.selectedFiles().first();
         QPainter painter;
         if(fileDialog.getFilenameExtension() == "pdf") {
             QPrinter pdfPrinter;
             pdfPrinter.setOutputFormat(QPrinter::PdfFormat);
             pdfPrinter.setPaperSize(geometryGraphicsView->size(), QPrinter::Point);
             pdfPrinter.setFullPage(true);
             pdfPrinter.setOutputFileName(filename);
             painter.begin(&pdfPrinter);
             geometryGraphicsView->render(&painter);
             painter.end();
         } else {
             QImage image(geometryGraphicsView->size(),  QImage::Format_ARGB32);
             image.fill(QColor(Qt::white).rgb());
             painter.setRenderHint(QPainter::Antialiasing);
             painter.begin(&image);
             geometryGraphicsView->render(&painter);
             painter.end();
             image.save(filename);
        }
     }
 }
