#ifndef GEOMETRYVIEW_H
#define GEOMETRYVIEW_H

#include <QtGui>
#include <vector>
#include "ViewModel/AbstractGeoShapeViewModel.h"
#include "View/AddLineVisualisation.h"
#include "View/SelectionTool.h"
#include "View/ScalingInfo.h"
#include "Model/GeometryModel.h"

using namespace std;

class GeometryModel;
class AddLineVisualisation;
class SelectionTool;

class GeometryView : public QGraphicsView
{
     Q_OBJECT

public:
    GeometryView(QWidget * parent=0);

    void setGeometryModel(GeometryModel * geometryModel) {
        this->geometryModel = geometryModel;
    }

    GeometryModel * getGeometryModel() {
        return this->geometryModel;
    }

    void scaleToViewPort();

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event) {} //nie uzywamy, ale lepiej zeby nie wolalo nadrzednej implementacji, bo wybuchnie



    virtual ~GeometryView();

    void resizeEvent(QResizeEvent *event);

signals:
    void enableDeletingShapes(bool isEnabled);
private slots:

    void showLabels(bool areShown);
    void deleteSelectedShapes();
    virtual void geometryChanged();
    void translateFigureAtPosition(QMouseEvent *event);
    void findGeoShapeToTransform(QMouseEvent *event);

private:
    void redrawGeometry();

    bool areLabelsShown;
    bool isLeftMouseButton;
    double currentScaleMultiplier;
    QGraphicsScene * graphicsScene;
    GeometryModel * geometryModel;
    QPointF sourcePosition;
    AbstractGeoShapeViewModel * currentGeoShapeVM;
    AddLineVisualisation * addLineVisualisation;
    SelectionTool * selectionTool;
    QGraphicsItem * currentlySelectedGraphicsItem;
    ScalingInfo *scalingInfo;
};

#endif // GEOMETRYVIEW_H
