#ifndef CUSTOMLINEANIMATION_H
#define CUSTOMLINEANIMATION_H

#include "ViewModel/PointVM.h"
#include "ViewModel/LineSegmentVM.h"
#include "Model/GeometryModel.h"

class AddLineVisualisation
{
public:
    AddLineVisualisation(GeometryModel * model, QGraphicsScene * scene, bool areLabelsShown, ScalingInfo *scalingInfo);
    ~AddLineVisualisation();

    void initNewLine(QPointF initPoint);
    void drag(QPointF point);
    void setLabelsShown(bool areLabelsShown) {
        this->areLabelsShown = areLabelsShown;
    }

private:
    bool areLabelsShown;
    GeometryModel * model;
    QGraphicsScene * scene;
    ScalingInfo *scalingInfo;

    PointVM * currentInitPoint;
    LineSegmentVM * currentLineSeg;


};

#endif // CUSTOMLINEANIMATION_H
