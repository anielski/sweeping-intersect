#ifndef SCALINGINFO_H
#define SCALINGINFO_H

struct ScalingInfo
{
public:
    ScalingInfo(double xMove, double yMove, double multiplier);

    double xMove;
    double yMove;
    double multiplier;
};

#endif // SCALINGINFO_H
