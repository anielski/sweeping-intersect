#ifndef SELECTIONTOOL_H
#define SELECTIONTOOL_H

#include <set>

#include "ViewModel/AbstractGeoShapeViewModel.h"
#include "Model/GeometryModel.h"

using namespace std;

class SelectionTool
{
public:
    SelectionTool(QGraphicsScene * scene);

    void initSelectionRect(QPointF point);
    void dragSelectionRect(QPointF point);
    void releaseSelectionRect(vector<AbstractGeoShapeViewModel *> geoShapes);


    void selectShape(AbstractGeoShapeViewModel * shape);
    void clearSelection();


    set<AbstractGeoShapeViewModel*> getSelectedShapes() {
        return selectedShapes;
    }
    int getSelectionCount() {
        return selectedShapes.size();
    }

private:
    QGraphicsScene * scene;
    set<AbstractGeoShapeViewModel*> selectedShapes;
    QGraphicsRectItem * currentRectItem;
};

#endif // SELECTIONTOOL_H
