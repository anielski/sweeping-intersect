#include "SelectionTool.h"

SelectionTool::SelectionTool(QGraphicsScene * scene) : scene(scene){}

void SelectionTool::initSelectionRect(QPointF point) {
    QBrush brush(QColor(0,0,255,50));
    QPen pen(brush,2,Qt::DashLine);
    currentRectItem = scene->addRect(point.x(), point.y(), point.x(), point.y(),pen,brush);
    currentRectItem->setZValue(SELECTION_RECT_Z);

}

void SelectionTool::dragSelectionRect(QPointF point) {
    QRectF rect = currentRectItem->rect();

    if(point.x() > rect.topLeft().x() && point.y() > rect.topLeft().y()) {
        rect.setBottomRight(point);
        currentRectItem->setRect(rect);
    }


}

void SelectionTool::releaseSelectionRect(vector<AbstractGeoShapeViewModel*> geoShapes) {

    QBrush brush = currentRectItem->brush();
    brush.setColor(QColor(0,0,255));
    QPen pen = currentRectItem->pen();
    pen.setBrush(brush);
    currentRectItem->setPen(pen);
    currentRectItem->setBrush(brush);

    for(int i=0; i<geoShapes.size(); i++) {
        if(geoShapes[i]->isObscuredBy(currentRectItem)) {
            selectShape(geoShapes[i]);
        }
    }

    scene->removeItem(currentRectItem);
    delete currentRectItem;
    currentRectItem = NULL;
}

void SelectionTool::selectShape(AbstractGeoShapeViewModel *shape) {
    shape->setSelected(true);
    selectedShapes.insert(shape);
}

void SelectionTool::clearSelection() {
    for(set<AbstractGeoShapeViewModel*>::iterator i=selectedShapes.begin(); i != selectedShapes.end(); ++i) {
        (*i)->setSelected(false);
    }
    selectedShapes.clear();
}
