#include "View/GeometryView.h"
#include "ViewModel/PointVM.h"

#include <ctime>
#include <iostream>
#include <limits>
#include <cmath>

using namespace std;

GeometryView::GeometryView(QWidget * parent) : QGraphicsView(parent) {
    graphicsScene = new QGraphicsScene();
    this->setSceneRect(this->geometry());
    setScene(graphicsScene);
    setRenderHint(QPainter::Antialiasing, true);
    this->areLabelsShown = false;
    this->isLeftMouseButton = false;
    this->currentScaleMultiplier = 1.0;

    selectionTool = new SelectionTool(graphicsScene);
    this->scalingInfo = new ScalingInfo(0,0,1);
}

void GeometryView::redrawGeometry() {

    graphicsScene->clear();
    vector<AbstractGeoShapeViewModel*> geoShapes = geometryModel->getGeoShapes();
    for(int i=0 ; i < geoShapes.size(); i++ ) {
        geoShapes[i]->setLabelVisible(areLabelsShown);
        geoShapes[i]->draw(graphicsScene);
    }

}

void GeometryView::geometryChanged() {
   scaleToViewPort();
   redrawGeometry();
}

void GeometryView::showLabels(bool areShown) {
    this->areLabelsShown = areShown;
    redrawGeometry();
}

void GeometryView::translateFigureAtPosition(QMouseEvent *event) {

    QPointF point = QPointF(event->pos());

    if(currentGeoShapeVM) {
            QPointF diff = QPointF(point.x() - sourcePosition.x(), point.y()- sourcePosition.y());
            currentGeoShapeVM->translate(currentlySelectedGraphicsItem, diff, scalingInfo);
            sourcePosition = point;
    }
    graphicsScene->update();
}

void GeometryView::findGeoShapeToTransform(QMouseEvent *event) {
    sourcePosition =  QPointF(event->pos());


    vector<AbstractGeoShapeViewModel*> geoShapes = geometryModel->getGeoShapes();

    for(int i=0 ; i < geoShapes.size(); i++ ) {
        QPointF point = mapToScene(event->pos());
        currentlySelectedGraphicsItem = geoShapes[i]->geoShapeAt(graphicsScene, point);
        if(currentlySelectedGraphicsItem) {
            currentGeoShapeVM = geoShapes[i];

            //Element wybrany do przesuniecia jest od razu zaznaczony
            selectionTool->clearSelection();
            selectionTool->selectShape(geoShapes[i]);
            return;
        }
    }

    //Jesli nie znalezlismy elementu do przesuniecia, to znaczy ze kliknelismy w proznie
    selectionTool->clearSelection(); //usuwamy dotychczasowe zaznaczenie
    selectionTool->initSelectionRect(mapToScene(event->pos())); //zaczynamy tworzyc ramke zaznaczania

    currentlySelectedGraphicsItem = NULL;
    currentGeoShapeVM = NULL;
}

void GeometryView::mousePressEvent(QMouseEvent *event) {
    //Lewym przyciskiem zaznaczamy lub przesuwamy elementy
    if (event->button() == Qt::LeftButton) {
        isLeftMouseButton = true;
        findGeoShapeToTransform(event);
    //Prawym przyciskiem rysujemy nowe odcinki/punkty
    } else {
        selectionTool->clearSelection();
        QPointF point = mapToScene(event->pos());
        addLineVisualisation = new AddLineVisualisation(geometryModel, graphicsScene, areLabelsShown, scalingInfo);
        addLineVisualisation->initNewLine(point); //na poczatku tylko punkt
    }

}

void GeometryView::mouseMoveEvent(QMouseEvent *event) {
    if(isLeftMouseButton) {
        if(currentlySelectedGraphicsItem) {
            translateFigureAtPosition(event); //jesli wybralismy pojedynczy element- przesuwamy go
        } else {
            selectionTool->dragSelectionRect(mapToScene(event->pos())); //wpp jestesmy w trybie zaznaczania obszarowego
        }
    } else {
        addLineVisualisation->drag(mapToScene(event->pos())); //ciagniemy nowa linie
    }
}

void GeometryView::mouseReleaseEvent(QMouseEvent *event) {
    if(isLeftMouseButton) {
        isLeftMouseButton = false;
        if(!currentlySelectedGraphicsItem) {
            //jesli bylismy w trybie zaznaczania obszarowego, obliczamy jego rezultaty
            selectionTool->releaseSelectionRect(geometryModel->getGeoShapes());
        }
        emit enableDeletingShapes((bool)selectionTool->getSelectionCount());
    } else {
        delete addLineVisualisation;
    }
}

GeometryView::~GeometryView() {
    delete graphicsScene;
    delete selectionTool;
    delete geometryModel;
    delete scalingInfo;
//    geometryModel->clearMemory();
}

void GeometryView::scaleToViewPort() {
    int x, y, w, h;
    geometry().getRect(&x, &y, &w, &h);

    vector<AbstractGeoShapeViewModel*> geoShapes = geometryModel->getGeoShapes();

    if(geoShapes.size()) {

    double bottom = numeric_limits<double>::max();
    double left = numeric_limits<double>::max();
    double top = numeric_limits<double>::min();
    double right = numeric_limits<double>::min();


    for(int i=0 ; i < geoShapes.size(); i++ ) {
        if (geoShapes[i]->isScalable()) { //proste nie sa brane pod uwage przy obliczaniu skali
            double testTop = geoShapes[i]->getTop();
            if(testTop > top) top = testTop;
            double testRight = geoShapes[i]->getRight();
            if(testRight > right) right = testRight;
            double testBottom = geoShapes[i]->getBottom();
            if(testBottom < bottom) bottom = testBottom;
            double testLeft = geoShapes[i]->getLeft();
            if(testLeft < left) left = testLeft;
        }
    }

    double xSpan = abs(right-left);
    double ySpan = abs(top-bottom);
    double renderRatio = xSpan/ySpan;
    double viewportRatio = ((double)w)/((double)h);

    double multiplier;
    if(viewportRatio < renderRatio) multiplier = (double)w/xSpan;
        else multiplier = (double)h/ySpan;
    multiplier*=0.9;

    double xMove = -((left+right)/2.0);
    double yMove = -((bottom+top)/2.0);

    for(int i=0 ; i < geoShapes.size(); i++ ) {
        geoShapes[i]->scaleViewPosition(xMove, yMove, multiplier); //proste rowniez sie skaluja i dopasowuja do viewportu
    }

    QPointF topLeft = mapToScene( 0, 0 );
    QPointF bottomRight = mapToScene(viewport()->width() - 1, viewport()->height() - 1 );
    QRectF rect = QRectF(topLeft, bottomRight );

    graphicsScene->setSceneRect(rect);
    currentScaleMultiplier = multiplier;
    scalingInfo->xMove = xMove;
    scalingInfo->yMove = yMove;
    scalingInfo->multiplier = multiplier;
    }
}

void GeometryView::resizeEvent(QResizeEvent *event) {
    QGraphicsView::resizeEvent(event);
    geometryChanged();
}

void GeometryView::deleteSelectedShapes() {

    set<AbstractGeoShapeViewModel*> selectedShapes = selectionTool->getSelectedShapes();
    selectionTool->clearSelection();

    for(set<AbstractGeoShapeViewModel*>::iterator i=selectedShapes.begin(); i != selectedShapes.end(); ++i) {
        (*i)->erase(graphicsScene);
        geometryModel->removeGeoShape(*i);
    }

    emit enableDeletingShapes(false);

}


