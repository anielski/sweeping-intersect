#ifndef SAVEDIALOG_H
#define SAVEDIALOG_H

#include <QFileDialog>


class SaveDialog : public QFileDialog
{
    Q_OBJECT
public:
    explicit SaveDialog(QWidget *parent = 0);
    QString getFilenameExtension() { return filenameExtension; }
    virtual void setNameFilters(const QStringList &filters);

protected slots:
    virtual void onNameFilterChanged(QString);
protected:
    QString filenameExtension;

};
#endif
