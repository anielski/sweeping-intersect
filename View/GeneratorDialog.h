#ifndef GENERATORDIALOG_H
#define GENERATORDIALOG_H

#include <QDialog>

#include <vector>
#include "ViewModel/AbstractGeoShapeViewModel.h"
#include "ViewModel/PointVM.h"
#include "ViewModel/LineSegmentVM.h"
#include "Model/Generator.h"
#include "ui_GeneratorDialog.h"

using namespace std;


class GeneratorDialog : public QDialog, public Ui::GeneratorDialog
{
    Q_OBJECT
    
public:
    explicit GeneratorDialog(QWidget *parent = 0);
    ~GeneratorDialog();

    vector<AbstractGeoShapeViewModel*> getResults() {
        return results;
    }
    
signals:
    void shapesGenerated();

private slots:
    void performShapesGeneration();

private:
    Ui::GeneratorDialog *ui;
    vector<AbstractGeoShapeViewModel*> results;
};

#endif // GENERATORDIALOG_H
