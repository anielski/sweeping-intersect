#include "GeneratorDialog.h"
#include "ui_GeneratorDialog.h"
#include <iostream>
GeneratorDialog::GeneratorDialog(QWidget *parent) :
    QDialog(parent) {

    setupUi(this);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(performShapesGeneration()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(close()));

}

GeneratorDialog::~GeneratorDialog()
{
    delete ui;
}

void GeneratorDialog::performShapesGeneration() {
    results.clear();
    if(pointRadioButton->isChecked()) {
        vector<PointVM*>* genPoints = Generator::generateInterval(nPointsSpinBox->value(),
                                                                 xMinSpinBox->value(), xMaxSpinBox->value(),
                                                                 yMinSpinBox->value(), yMaxSpinBox->value());
        results.assign(genPoints->begin(), genPoints->end());
    } else if(lineSegRadioButton->isChecked()) {
        vector<PointVM*>* genPoints = Generator::generateInterval(nPointsSpinBox->value()*2,
                                                                 xMinSpinBox->value(), xMaxSpinBox->value(),
                                                                 yMinSpinBox->value(), yMaxSpinBox->value());
        for(int i=0; i< genPoints->size() -1 ; i+=2) {
            PointVM* p1 = (*genPoints)[i];
            PointVM* p2 = (*genPoints)[i+1];

            results.push_back(new LineSegmentVM(p1->getPosition().x(), p1->getPosition().y(),
                                                p2->getPosition().x(), p2->getPosition().y()));
        }
    }

    emit shapesGenerated();
    close();
}
