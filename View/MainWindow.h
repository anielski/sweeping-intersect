#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "GeometryView.h"

#include "ui_mainwindow.h"
#include "View/GeneratorDialog.h"

class QAction;
class QMenu;

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);

    void setGeometryModel(GeometryModel *model);
    ~MainWindow();

private slots:
    void open();
    bool save();
    bool saveAs();

    void on_actionSave_as_image_triggered();
    void showGeneratedShapes();
    
private:
    Ui::MainWindow *ui;
    GeneratorDialog * generatorDialog;
    GeometryModel * geometryModel;

    void createActions();
    void createMenu();
    bool callMeMaybeSave();
    void loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);

    QString curFile;

    QMenu *fileMenu;

};

#endif // MAINWINDOW_H
