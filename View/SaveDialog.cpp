#include "View/SaveDialog.h"

SaveDialog::SaveDialog(QWidget *parent) :
    QFileDialog(parent)
{
    this->setFileMode(QFileDialog::AnyFile);
    this->setAcceptMode(QFileDialog::AcceptSave);
    connect(this, SIGNAL(filterSelected(QString)), SLOT(onNameFilterChanged(QString)));
}

void SaveDialog::onNameFilterChanged(QString newFilter)
{
    filenameExtension = newFilter.section('.', -1);
    filenameExtension.chop(1);
    this->setDefaultSuffix(filenameExtension);
}

void SaveDialog::setNameFilters(const QStringList &filters) {
    QFileDialog::setNameFilters(filters);
    onNameFilterChanged(filters[0]);
}
