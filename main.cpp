#include <QtGui/QApplication>
#include "View/MainWindow.h"

#include "Model/GeometryModel.h"
#include "ViewModel/PointVM.h"
#include "ViewModel/LineSegmentVM.h"
#include "ViewModel/PolygonVM.h"
#include "ViewModel/StraightLineVM.h"
#include "Model/Generator.h"
#include "sweepalgorithm.h"
#include <Segment.h>

#include <ctime>



void test1(GeometryModel& model) {
    srand(time(NULL));

    for(int i=0; i<1000; i++) {
        double x = (rand()/(double)(RAND_MAX + 1)) * 200 - 100;
        double y = (rand()/(double)(RAND_MAX + 1)) * 200 - 100;
        PointVM * p = new PointVM(x,y);
        model.addGeoShape(p);
    }

    LineSegmentVM * lineSeg = new LineSegmentVM(-1.0, 0.0, 1.0, 0.1);
    lineSeg->setColor(QColor(255,0,0));
    model.addGeoShape(lineSeg);
}

void test2(GeometryModel& model) {
    vector<PointVM *> * vec = Generator::generateOnCircle(100, 0, 0, 10);
    model.addPoints(vec);
}

void test3(GeometryModel& model) {
    vector<PointVM *> * vec = Generator::generateOnCircuit(100, -10, 0, 10, 0, 10, 10, 0 , 10);
    model.addPoints(vec);
}

void test4(GeometryModel& model) {
    vector<PointVM *> * vec = Generator::generateOnLine(100, -100, 100, 0, 0, 10, 1);
    model.addPoints(vec);
}

void test5(GeometryModel& model) {
    vector<PointVM *> * vec = Generator::generateOnSquareAndAxes(100, 100, 0, 0, 10, 0, 10, 10, 0, 10);
    model.addPoints(vec);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    GeometryModel geometryModel;

    MainWindow w;
    w.setGeometryModel(&geometryModel);
    w.setWindowTitle("Your3000 Revamped Edition");
    w.show();

    vector<Segment*> segs;
    segs.push_back(new Segment(2, -3, 6, -2));
//    segs.push_back(new Segment(1, -1, 3, 2));
//    segs.push_back(new Segment(4, -5, 5, -1));
//    segs.push_back(new Segment(3, -4, 6, -4));
//    segs.push_back(new Segment(4, -1, 7, -4));


    for (int i =0; i<segs.size();i++){
        geometryModel.addGeoShape(segs.at(i)->getVM());
    }

    SweepAlgorithm* salg = new SweepAlgorithm(&geometryModel);//&geometryModel);

    QObject::connect(&(*w.nextButton), SIGNAL(clicked()), &(*salg), SLOT(nextStep()));
    QObject::connect(&(*w.resetButton), SIGNAL(clicked()), &(*salg), SLOT(reset()));

    geometryModel.performViewUpdate();
    return a.exec();
}
