#ifndef SEGMENT_H
#define SEGMENT_H

#include <set>
#include <Model/GeometryModel.h>

#define SEGTODO 0
#define SEGCURR 1
#define SEGDONE 2

class Segment;

class Point {
public:
    PointVM* pvm;

    double x;
    double y;
    bool isLeft;
    bool isInter;
    Segment* parent;
    Segment* parent2;

    Point(double xp, double yp, Segment* par, bool left);
    Point(double xp, double yp, Segment* par1, Segment* par2);
    ~Point();

    bool operator < (const Point& p2) const;
    PointVM* getVM();
    double det(Point* a, Point* b);
};




class Segment {

    static int currInd;

public:
    set<Segment*> checked;
    LineSegmentVM* segvm;
    Segment* lower;
    Segment* upper;
    Point* left;
    Point* right;
    int ind;
    int state;

    Segment(double x1, double y1, double x2, double y2);
    Segment(LineSegmentVM* existing);
    ~Segment();

    LineSegmentVM* getVM();
    Point* intersects(Segment* s2);
    double yInterWithSweep();
};

#endif // SEGMENT_H
