#-------------------------------------------------
#
# Project created by QtCreator 2012-10-18T01:12:40
#
#-------------------------------------------------

QT       += core gui

TARGET = Your3000RE
TEMPLATE = app


SOURCES += main.cpp \
    View/MainWindow.cpp \
    ViewModel/AbstractGeoShapeViewModel.cpp \
    Model/GeometryModel.cpp \
    ViewModel/PointVM.cpp \
    ViewModel/LineSegmentVM.cpp \
    View/GeometryView.cpp \
    ViewModel/PolygonVM.cpp \
    ViewModel/PolygonLineSegmentVM.cpp \
    ViewModel/StraightLineVM.cpp \
    Model/FileParser.cpp \
    View/AddLineVisualisation.cpp \
    View/SelectionTool.cpp \
    Model/Generator.cpp \
    View/GeneratorDialog.cpp\
    View/ScalingInfo.cpp \
    View/SaveDialog.cpp \
    sweepalgorithm.cpp \
    Segment.cpp \
    sweepstate.cpp


HEADERS  += View/MainWindow.h \
    ViewModel/AbstractGeoShapeViewModel.h \
    View/GeometryView.h \
    Model/GeometryModel.h \
    ViewModel/PointVM.h \
    ViewModel/LineSegmentVM.h \
    ViewModel/PolygonVM.h \
    ViewModel/PolygonLineSegmentVM.h \
    ViewModel/StraightLineVM.h \
    Model/FileParser.h \
    View/AddLineVisualisation.h \
    View/SelectionTool.h \
    Model/Generator.h \
    View/GeneratorDialog.h \
    View/ScalingInfo.h \
    View/SaveDialog.h \
    sweepalgorithm.h \
    Segment.h \
    sweepstate.h


FORMS    += mainwindow.ui \
    View/GeneratorDialog.ui



