#include "Segment.h"
#include "sweepalgorithm.h"


int Segment::currInd = 0;

Point::Point(double xp, double yp, Segment* par, bool left){
    x = xp;
    y = yp;
    parent = par;
    isLeft = left;
    isInter = false;

    pvm = new PointVM(xp,yp);
}

Point::Point(double xp, double yp, Segment* par1, Segment* par2){
    x = xp;
    y = yp;
    isInter = true;
    parent = par1;
    parent2 = par2;
    pvm = new PointVM(xp,yp);
}

bool Point::operator < (const Point& p2) const {
    return (this->x < p2.x);
}

Point::~Point(){
    delete pvm;
}

PointVM* Point::getVM(){
    return pvm;
}

double Point::det(Point* a, Point* b){
   Point* c = this;
   return a->x*b->y + a->y*c->x + b->x*c->y
           - (c->x*b->y + a->x*c->y + a->y*b->x);
}


Segment::Segment(double x1, double y1, double x2, double y2){
    if (x1 < x2){
        left = new Point(x1,y1, this, true);
        right = new Point(x2,y2, this, false);
    } else if (x1 > x2)
    {
        right = new Point(x1,y1,this,false);
        left = new Point(x2,y2,this,true);
    } else {
        cerr << "vertical segment error" << endl;
    }
    ind = currInd++;
    state = SEGTODO;
    lower = NULL;
    upper = NULL;
    segvm = new LineSegmentVM(x1,y1,x2,y2);
}

Segment::Segment(LineSegmentVM* existing){
    double x1 = existing->getPointA().getPosition().x();
    double y1 = existing->getPointA().getPosition().y();
    double x2 = existing->getPointB().getPosition().x();
    double y2 = existing->getPointB().getPosition().y();

    if (x1 < x2){
        left = new Point(x1,y1, this, true);
        right = new Point(x2,y2, this, false);
    } else if (x1 > x2)
    {
        right = new Point(x1,y1,this,false);
        left = new Point(x2,y2,this,true);
    } else {
        cerr << "verticla segment error" << endl;
    }
    state = SEGTODO;
    ind = currInd++;
    lower = NULL;
    upper = NULL;
    segvm = existing;
}

Segment::~Segment(){
    delete left;
    delete right;
    delete segvm;
}

LineSegmentVM* Segment::getVM(){
    return segvm;
}

Point* Segment::intersects(Segment* s2) {

    if(checked.find(s2)!=checked.end()){
        return NULL;
    }

    checked.insert(s2);
    s2->checked.insert(this);

    Point* l2 = s2->left;
    Point* r2 = s2->right;
    Point* l1 = left;
    Point* r1 = right;

    double d1 = l1->det(l2,r2);
    double d2 = r1->det(l2,r2);
    double d3 = l2->det(l1,r1);
    double d4 = r2->det(l1,r1);
    if ((d1*d2 < 0) && (d3*d4<0)) {
        //punkt przeciecia dwoch prostych, wktorych sa zawarte odcinki
        // k1 :
        double A1,A2,B1,B2,C1,C2;
        A1 = l1->y - r1->y; B1 = r1->x - l1->x; C1 = l1->x*r1->y - l1->y*r1->x;
        A2 = l2->y - r2->y; B2 = r2->x - l2->x; C2 = l2->x*r2->y - l2->y*r2->x;
        //double yp = (C2*A1 - C1*A2) / (B1*A2 - B2*A1);
        //double xp = (B1*yp + C1)/(-A1);
        double xp = (C2*B1 - C1*B2) / (A1*B2 - B1*A2);
        double yp = (A1*xp + C1)/(-B1); // nie ma odcinkow pionowych, wiec B1!=0
        return new Point(xp,yp,this,s2);
    }
    // bez przypadkow  szczegolnych
    else return NULL;

}

double Segment::yInterWithSweep(){
    double a,b,c,d;
    a = this->left->x; b = this->left->y; c = this->right->x; d = this->right->y;
    return (b*c - a*d + SweepAlgorithm::sweepLine * (d - b))/ (c - a);
}
