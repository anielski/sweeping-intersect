#include "sweepalgorithm.h"


bool segmcomp::operator() (int s1, int s2){
    int i1 = SweepAlgorithm::segmap[s1]->ind;
    int i2 = SweepAlgorithm::segmap[s2]->ind;
    double l = SweepAlgorithm::sweepLine;
    return (SweepAlgorithm::segmap[s1]->yInterWithSweep()
          < SweepAlgorithm::segmap[s2]->yInterWithSweep());
}

SweepAlgorithm::~SweepAlgorithm(){

    int size = segs.size();
    for(int i=0;i<size;i++){
        delete segs.at(i);
    }
    size = resultPoints.size();
    for(int i=0;i<size;i++){
        delete resultPoints.at(i);
    }

}

SweepAlgorithm::SweepAlgorithm(GeometryModel* _model) :
    sweepVM(StraightLineVM(-1, 0, 0)),
    algoStarted(false), algoEnded(true),
    cpts(QColor(0,0,0)),
    cSegToDo(QColor(0,0,0)),
    cSegCurr(QColor(255,0,0)),
    cSegDone(QColor(0,0,255)),
    cSweep(QColor(200,200,200)),
    model(_model){

    model->addGeoShape(&sweepVM);

    sweepVM.setColor(cSweep);
}

void SweepAlgorithm::setSegments(vector<Segment*> s){
    //this->segs.clear();
    for(int i=0;i<s.size();i++){
        Qevents.insert(s.at(i)->left);
        Qevents.insert(s.at(i)->right);
        segmap.insert(pair<int,Segment*>(s.at(i)->ind,s.at(i)));
        setSegmentState(s.at(i), SEGTODO);
    }
    double c=0.0;
    if (s.size()>0){
        Point* p = *(Qevents.begin());
        c = p->x;
    }

    set<Point*>::iterator it = Qevents.begin();
    for(;it != Qevents.end();++it){
        cerr<< (*it)->x << ", " << (*it)->y << endl;
    }
}

double SweepAlgorithm::sweepLine;
map<int,Segment*> SweepAlgorithm::segmap;


void SweepAlgorithm::start(){
    algoStarted = true;
    algoEnded = false;
    fetchAllSegments();

    for(vector<Point*>::iterator it = resultPoints.begin(); it != resultPoints.end();++it){
        model->removeGeoShape((*it)->getVM());
    }
    resultPoints.clear();

}

void SweepAlgorithm::end(){
    algoStarted = false;
    algoEnded = true;

    cerr << "Total number of intersections:" << resultPoints.size() << endl;
    for(int i=0;i<resultPoints.size();i++){
        //resultPoints.at(i)->getVM()->setColor(cpts);
        //model->addGeoShape(resultPoints.at(i)->getVM());
        cerr << "IntPoint: (" << resultPoints.at(i)->x << ", " << resultPoints.at(i)->y << ")" << endl;
    }

    model->performViewUpdate();
}

void SweepAlgorithm::nextStep(){

    if (!algoStarted){
        start();
    }

    if (Qevents.empty()){
        end();
        return;
    }

    Point* pi = (*(Qevents.begin()));

    sweepLine = pi->x;
    sweepVM.setC(sweepLine);

//    cerr << "current sweepline: " << sweepLine << endl;

    Qevents.erase(Qevents.begin());

    if (pi->isInter){
        // s1 i s2 przecinaja sie w pi
        // zmien porzadek s1 i s2 w T
        Tstate.swap(pi->parent->ind,pi->parent2->ind);

        // wykryj przeciecia i zaktualizuj Q
        if (pi->parent->upper != NULL)
            intersect(pi->parent, pi->parent->upper);
        if (pi->parent2->lower != NULL)
            intersect(pi->parent2->lower, pi->parent2);

    }
    // zaktualizuj T
    else if (pi->isLeft){
        // dodaj odcinek do T w porzadku
        Tstate.insert(pi->parent->ind);
        setSegmentState(pi->parent, SEGCURR);

        // wykryj przeciecia i zaktualizuj Q
        if (pi->parent->upper != NULL)
            intersect(pi->parent, pi->parent->upper);
        if (pi->parent->lower != NULL)
            intersect(pi->parent->lower, pi->parent);

    } else if (!pi->isLeft){

        // wykryj przeciecia i zaktualizuj Q
        if (pi->parent->upper != NULL && pi->parent->lower != NULL){
            intersect(pi->parent->lower, pi->parent->upper);
        }

         // usun odcinek z T
         Tstate.remove(pi->parent->ind);
         setSegmentState(pi->parent, SEGDONE);
    }

    model->performViewUpdate();
    return;
}

void SweepAlgorithm::intersect(Segment* lower, Segment* upper){
//    cerr<< "intersecting "<< lower->ind << ", " << upper->ind << ""<<endl;

    // sprawdzenie czy juz sprawdzalismy
    // przecinanie dla tych dwoch odcinkow
    // dokonuje sie w intersects

    Point* intPoint = lower->intersects(upper);

    if(intPoint != NULL){
        //istnieje co najmniej jedna para odcinkow
        // ktore sie tna --> cerr<<"SUCCESS"<<endl; return; exit;

        Qevents.insert(intPoint);
        resultPoints.push_back(intPoint);

        intPoint->getVM()->setColor(cpts);
        model->addGeoShape(intPoint->getVM());

//        cerr << "added intersect point (" <<intPoint->x << ", " << intPoint->y << ")" << endl;
    }

}


void SweepAlgorithm::fetchAllSegments(){
    segs.clear();

    vector<AbstractGeoShapeViewModel*> shapes = model->getGeoShapes();
    for (int i=0; i < shapes.size(); i++){
        LineSegmentVM* v;
        if (!shapes.at(i)->isScalable()){
            continue;
        }
        v = dynamic_cast<LineSegmentVM*>(shapes.at(i));
        if (v != NULL){
            segs.push_back(new Segment(v));
        }
    }
    this->setSegments(segs);
}

void SweepAlgorithm::sweepFromAll(){

    fetchAllSegments();
    sweep();
}

// cala funkcja, nierozbita na kroki do wizualizacji
void SweepAlgorithm::sweep(){

    vector<Point*> res;

    while(!Qevents.empty()){
        Point* pi = (*(Qevents.begin()));

        sweepLine = pi->x;

        Qevents.erase(Qevents.begin());

        if (pi->isInter){
            // s1 i s2 przecinaja sie w pi
            // zmien porzadek s1 i s2 w T
            Tstate.swap(pi->parent->ind,pi->parent2->ind);
        }
        // zaktualizuj T
        else if (pi->isLeft){
            // dodaj odcinek do T w porzadku
            Tstate.insert(pi->parent->ind);
//            pi->parent->state = SEGCURR;
            setSegmentState(pi->parent, SEGCURR);
        }
        else if (!pi->isLeft){
            // usun odcinek z T
            //Tstate.erase(Tstate.find(pi->parent->ind));
            Tstate.remove(pi->parent->ind);
            //pi->parent->state = SEGDONE;
            setSegmentState(pi->parent, SEGDONE);
        }

        // zaktualizuj Q
        vector<int>::iterator it = Tstate.begin();
        int sprev;
        if (Tstate.size()>0){
            sprev = *it;
            ++it;
        }
        for(;it != Tstate.end();++it){
//            cerr<< "intersecting "<<sprev << ", " << (*it) << ""<<endl;

            // sprawdzenie czy juz sprawdzalismy
            // przecinanie dla tych dwoch odcinkow
            // dokonuje sie w intersects

            Segment* s1 = SweepAlgorithm::segmap[sprev];
            Segment* s2 = SweepAlgorithm::segmap[*it];

            Point* intPoint = s1->intersects(s2);

            if(intPoint != NULL){
                // wersa algorytmu z punktu 2
                // istnieje co najmniej jedna para odcinkow
                // ktore sie tna --> cerr<<"SUCCESS"<<endl; return;

                Qevents.insert(intPoint);
                res.push_back(intPoint);

//                cerr << "added intersect point (" <<intPoint->x << ", " << intPoint->y << ")" << endl;
            }

            sprev = *it;
        }


    }

    QColor cpts = QColor(255,0,255);
    for(int i=0;i<res.size();i++){
        res.at(i)->getVM()->setColor(cpts);
        model->addGeoShape(res.at(i)->getVM());
    }

    model->performViewUpdate();
    return;// res;
}

void SweepAlgorithm::testIntersects(){
    cerr <<"testing"<<endl;

    vector<AbstractGeoShapeViewModel*> shapes = model->getGeoShapes();
    vector<Segment*> segs;
    for (int i=0; i < shapes.size(); i++){
        LineSegmentVM* v;
        v = dynamic_cast<LineSegmentVM*>(shapes.at(i));
        if (v != NULL){
            segs.push_back(new Segment(v));
        }
    }

    QColor cpts = QColor(255,0,255);

    int size = segs.size();
    for(int i=0; i<size; i++){
        for(int j=i; j<size; j++){
            Point* p = segs.at(i)->intersects(segs.at(j));
            if (p != NULL){
                p->pvm->setColor(cpts);
                this->model->addGeoShape(p->pvm);
                cerr<<"hello intersection;" << endl;
            }
            else {
                cerr <<"not this time"<<endl;
            }
        }
    }

    model->performViewUpdate();


}

void SweepAlgorithm::reset(){
    model->clearModel();

    algoEnded = true;
    algoStarted = false;
    segs.clear();
}

void SweepAlgorithm::setSegmentState(Segment *s, int state){

    s->state = state;

        if (state == SEGTODO){
           s->getVM()->setColor(cSegToDo);
        }
        else if (state == SEGCURR){
            s->getVM()->setColor(cSegCurr);
        }
        else if (state == SEGDONE){
            s->getVM()->setColor(cSegDone);
        }

}
