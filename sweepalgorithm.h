#ifndef SWEEPALGORITHM_H
#define SWEEPALGORITHM_H

#include <QObject>
#include <vector>
#include <ViewModel/LineSegmentVM.h>
#include <ViewModel/StraightLineVM.h>
#include <Segment.h>
#include <set>
#include "sweepstate.h"

using namespace std;

//bclass SweepAlgorithm;

struct pointcomp {
    bool operator() (const Point* p1, const Point* p2){
        if (p1->x < p2->x) return true;
        if (p1->x > p2->x) return false;
        return (p1->y < p2->y);
    }
};

struct segmcomp {
    bool operator() (int s1, int s2);

//    bool operator() (Segment* s1, Segment* s2);
/*{
        return (s1->yInterWithSweep()
                < s2->yInterWithSweep());
    }*/
};

class SweepAlgorithm : public QObject
{
    Q_OBJECT

public:
    bool algoEnded;
    bool algoStarted;
    static double sweepLine;
//    static vector<Segment*> segvec;
    static map<int,Segment*> segmap;
    //Point* lastInt;

    QColor cSegDone;
    QColor cSegCurr;
    QColor cSegToDo;
    QColor cpts;
    QColor cSweep;

    vector<Point*> resultPoints;

    vector<Segment*> segsDone;
    //vector<Segment*> segsCurr;
    //vector<Segment*> segsToDo;

    StraightLineVM sweepVM;
    //LineSegmentVM sweepVM2;

    set<Point*,pointcomp> Qevents;
    SweepState Tstate;

    vector<Segment*> segs;

    GeometryModel* model;
    SweepAlgorithm(GeometryModel* _model);
    ~SweepAlgorithm();

    void setSegments(vector<Segment*> s);
    void fetchAllSegments();

    void setSegmentState(Segment* s, int state);

    void intersect(Segment* lower, Segment* upper);

    void start();
    void end();

signals:

public slots:

    void nextStep();

    void testIntersects();
    void sweep();
    void sweepFromAll();

    void reset();

};

#endif // SWEEPALGORITHM_H
