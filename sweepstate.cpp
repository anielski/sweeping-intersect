#include "sweepstate.h"
#include "sweepalgorithm.h"
#include <iostream>

SweepState::SweepState(){ upper = 0; lower = 0;  }

bool SweepState::comp(int s1, int s2) {
    //int i1 = SweepAlgorithm::segmap.at(s1)->ind;
    //int i2 = SweepAlgorithm::segmap.at(s2)->ind;
    //double l = SweepAlgorithm::sweepLine;
    double y1 = SweepAlgorithm::segmap.at(s1)->yInterWithSweep();
    double y2 = SweepAlgorithm::segmap.at(s2)->yInterWithSweep();
    return y1 < y2;
}

void SweepState::insert(int n){
    vector<int>::iterator it = current.begin();

    //current.insert(it, n);

    int i = 0;
    while (it != current.end()){
        if (comp(n,*it)){

            //if (it != current.begin()){
            if (i > 0){
                Segment* s1 = SweepAlgorithm::segmap.at(current[i-1]);//*(it-1));
                Segment* s2 = SweepAlgorithm::segmap.at(n);
                Segment* s3 = SweepAlgorithm::segmap.at(current[i]);//*(it+1));
                s1->upper = s2;
                s2->lower = s1;
                s2->upper = s3;
                s3->lower = s2;
            }
            else { //
                Segment* s2 = SweepAlgorithm::segmap.at(n);
                Segment* s3 = SweepAlgorithm::segmap.at(current[i]);//*(it+1));

                s2->upper = s3;
                s3->lower = s2;
            }

            current.insert(it,n);
//            cerr << "CURRENT: " << n <<endl;
            return;
        }
//        previt = it;
        ++it;
        i++;
    }

    current.push_back(n);
    int sz = current.size();
    if (sz - 2 >= 0){
        Segment* s1 = SweepAlgorithm::segmap.at(current.at(sz-2));
        Segment* s2 = SweepAlgorithm::segmap.at(current.at(sz-1));
        s1->upper = s2;
        s2->lower = s1;
    }
}

void SweepState::remove(int n){
    for (vector<int>::iterator it = current.begin(); it != current.end(); ++it){
        if (*it == n){
            Segment* removed = SweepAlgorithm::segmap.at(n);
            if (removed->upper != NULL){
                removed->upper->lower = removed->lower;
            }
            if (removed->lower != NULL){
                removed->lower->upper = removed->upper;
            }

            removed->lower = NULL;
            removed->upper = NULL;

            current.erase(it);
//            cerr << "DONE: " << n <<endl;
            return;
        }
    }
}

int SweepState::size(){return current.size();}

void SweepState::swap(int n, int m){
    int size = current.size();
    // TODO
    int i,j;
    // i = binFind(n);

    for(i=0;i<size;i++){
        if(current[i] == n){

            j = i+1;
            if (j==size){
                cerr <<"lowerupperswapping error"<< endl;
            }

            Segment* s1 = SweepAlgorithm::segmap.at(n);
            Segment* s2 = SweepAlgorithm::segmap.at(current[j]);

            if (s1->lower != NULL) s1->lower->upper = s2;
            if (s2->upper != NULL) s2->upper->lower = s1;

            s2->lower = s1->lower;
            s1->upper = s2->upper;

            s1->lower = s2;
            s2->upper = s1;

            current[i]=m;
            current[j]=n;
//            cerr << "swapped succesfully "<< n<< ", " << m << endl;

            return;

        }
    }
}

vector<int>::iterator SweepState::begin(){return current.begin();}

vector<int>::iterator SweepState::end(){return current.end();}
