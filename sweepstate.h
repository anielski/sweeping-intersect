#ifndef SWEEPSTATE_H
#define SWEEPSTATE_H

#include <vector>

using namespace std;

class SweepState
{
    vector<int> current;

    bool comp(int s1, int s2);

public:

    int upper;
    int lower;

    SweepState();

    int size();
    void insert(int n);
    void remove(int n);
    void swap(int n, int m);
    vector<int>::iterator begin();
    vector<int>::iterator end();

};

#endif // SWEEPSTATE_H
